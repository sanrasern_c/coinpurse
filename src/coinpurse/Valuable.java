package coinpurse;

/**
 * It 's a interface for give a value of class that implements.
 * @author Sanrasern 5710547247
 *
 */
public interface Valuable extends Comparable<Valuable> {
	public double getValue();
	
}
