package coinpurse;
/**
 * Banknote with a monetary value.
 * You can't change the value of a banknote.
 * @author Sanrasern5710547247
 *
 */
public class BankNote extends AbstractValuable{
	/**
	 * value of the banknote.
	 * serialNumber of the banknote.
	 * nextSerialNumber of the banknote.
	 */
	private double value;
	private String currency;
	private int serialNumber;
	private static int nextSerialNumber;
	
	/**
	 * Constructor for a new banknote. 
	 * @param value is value of the banknote.
	 */
	public BankNote(double value , String currency){
		super(currency);
		nextSerialNumber ++;
		this.value = value;
		this.serialNumber = getNextSerialNumber();
	}
	/**
	 * To set serialNumber.
	 * @return Integer of next number of serialNumber.
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber;
	}
	/**
	 * To show value details.
	 * @return double of value of banknote.
	 */
	public double getValue(){
		return this.value;
	}
	/**
	 * To show details of banknote.
	 * @return String of details.
	 */
	public String toString(){
		return String.format("%.0f %s BankNote", this.getValue(), this.getCurrency());
	}
}
