package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

public class GreedyWithdraw implements WithdrawStrategy{
	public Valuable[] withdraw( double amount, List<Valuable> valuables ) {
		Collections.sort(valuables);
		List<Valuable> bucket = new ArrayList<Valuable>();
		for(int i=valuables.size()-1; i>=0; i--){
			if(amount - valuables.get(i).getValue() >= 0){
				bucket.add(valuables.get(i));
				amount -= valuables.get(i).getValue();
			}

		}
		if(amount == 0){
			for(int i=0; i<bucket.size(); i++){
				for(int j=0; j<valuables.size();j++){
					if(bucket.get(i).getValue() == valuables.get(j).getValue()){
						valuables.remove(j);
						break;
					}

				}
			}
			Valuable[] c = new Valuable[bucket.size()];
			bucket.toArray(c);
			return c;
		}
		return null;

	}
}
