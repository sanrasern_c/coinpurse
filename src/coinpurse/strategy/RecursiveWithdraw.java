package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.strategy.WithdrawStrategy;

/**
 * To find out which valuables are appropriate to withdraw.
 * @author Sanrasern Chaihetphon 5710547247
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	
	/**
	 * tempAmount is a amount when call helpWithdraw.
	 * count is a number that use when call helpWithdraw.
	 * collect is a list of the valuable.
	 */
	private double tempAmount = 0;
	private int count = 1;
	private List<Valuable> collect = new ArrayList<Valuable>();

	/**
	 * The main method to find appropriate of withdraw value.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		tempAmount = amount;
		List<Valuable> newList = new ArrayList<Valuable>();
		for(int i=0; i<valuables.size(); i++){
			newList.add(valuables.get(i));
		}
		Collections.sort(newList);
		
		int size = newList.size();
		List<Valuable> helpCollect = helpWithdraw(amount, newList, size-1);
		Valuable[] goWithdraw = new Valuable[helpCollect.size()];
		helpCollect.toArray(goWithdraw);
		return goWithdraw;
	}
	
	/**
	 * The helper method that find appropriate value to withdraw.
	 * @param amount is the remaining value of withdraw value.
	 * @param valuables is a list collect valuable.
	 * @param lastIndex is show the position of last index in the list.
	 * @return appropriate valuables to withdraw.
	 */
	public List<Valuable> helpWithdraw(double amount, List<Valuable> valuables, int lastIndex) {
		if(amount == 0){
			return collect;
		}
		if(lastIndex < 0 && amount != 0){
			count++;
			collect = new ArrayList<Valuable>();
			List<Valuable> helpCollect = helpWithdraw(tempAmount, valuables, valuables.size()-count);
			return helpCollect;
		}
		else{
			if(amount >= valuables.get(0).getValue()) {
				amount -= valuables.get(lastIndex).getValue();
				collect.add(0,valuables.get(lastIndex));
				lastIndex--;
				return helpWithdraw(amount, valuables, lastIndex);
			}
			else{
				amount += valuables.get(lastIndex+1).getValue();
				collect.remove(0);
				return helpWithdraw(amount, valuables, lastIndex);
			}
		}
	}
	
	
}
