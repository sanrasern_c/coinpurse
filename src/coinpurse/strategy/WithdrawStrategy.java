package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

public interface WithdrawStrategy {
	/**
	 * 
	 * @param amount 
	 * @param valuables
	 * @return 
	 */
	public Valuable[] withdraw (double amount , List<Valuable> valuables);
	
}
