package coinpurse;  

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the purse decides which
 *  money to remove.
 *  
 *  @author sanrasern 5710547247
 */
public class Purse extends Observable {
	
	/** Collection of money in the purse. */
	private List<Valuable> money = new ArrayList<Valuable>();
	/**
	 * use to sort in the money's array
	 */
	private final ValueComparator comparator = new ValueComparator();
	private WithdrawStrategy strategy;


	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** 
	 *  Create a purse with a specified capacity. 
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {

		this.capacity = capacity;
		money = new ArrayList<Valuable>();

	}

	/**
	 * Count and return the number of money in the purse.
	 * This is the number of money, not their value.
	 * @return the number of money in the purse.
	 */
	public int count() { 
		return money.size();
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public int getBalance(){
		int sum = 0;
		for(int i=0; i<money.size(); i++){
			sum += money.get(i).getValue();
		}
		return sum;
	}

	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy = strategy;
	}

	/**
	 * Return the capacity of the purse.
	 * @return the capacity.
	 */


	public int getCapacity() { return capacity; }

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {

		if(money.size() == capacity || money.size() > capacity)
			return true;
		else
			return false;
	}

	/** 
	 * Insert a money into the purse.
	 * The money is only inserted if the purse has space for it
	 * and the money has positive value.  
	 * @param money is a Coin object to insert into purse
	 * @return true if money inserted, false if can't insert
	 */
	public boolean insert( Valuable valuable ) {

		if(isFull() || valuable.getValue() <=0 ){
			return false;
		}

		else{
			money.add(valuable);
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}

	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Valuable withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		Valuable [] withdrawn = null;
		if(amount <= 0)
			return null;
		if(amount <= this.getBalance()){
			setWithdrawStrategy(new RecursiveWithdraw());
			withdrawn = strategy.withdraw(amount, money);
			if(withdrawn!=null){
				for(int i=0; i<withdrawn.length; i++){
				money.remove(withdrawn[i]);
				}
			}
			
			
		}
		super.setChanged();
		super.notifyObservers(this);
		return withdrawn;
	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 */
	public String toString() {

		return capacity + "coins with value " + this.getBalance();
	}

}

