package coinpurse;

import java.util.ResourceBundle;

public abstract class MoneyFactory {
	private static MoneyFactory moneyFactory = null;
	static MoneyFactory getInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if(moneyFactory == null) {
			ResourceBundle bundle = ResourceBundle.getBundle("purse");
			String factoryStr = bundle.getString("moneyFactory");
			moneyFactory = (MoneyFactory)Class.forName(factoryStr).newInstance();
		}
		return moneyFactory;
	}
	abstract Valuable createMoney(double value);
	
	Valuable createMoney(String value) {
		double newValue = Double.parseDouble(value);
		return moneyFactory.createMoney(value);
		
	}
}
