package coinpurse; 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author sanrasern5710547247
 */
public class PurseTest {

    /**
     * To test program.
     * @param args not used
     */
    public static void main( String[] args ) {
    	 /** Create a purse with capacity 5. */
    	 Purse purse = new Purse( 5 );
    	 /** Create a coin that value 10*/
    	 Coin coin1 = new Coin( 10 , "Baht" );
    	 /** Create a coupon that color is blue*/
    	 Coupon c = new Coupon("blue");
    	 /** Show result when insert coin1's coin to purse*/
    	 System.out.println(purse.insert( coin1 ));
    	 /** Show result when insert c's coin into purse*/
    	 System.out.println(purse.insert( c ));
    	 /** Show balance of the purse*/
    	 System.out.println(purse.getBalance());
    	 /** Show number of valuable in the purse*/
     	 System.out.println(purse.count());
    	 Valuable[] stuff = purse.withdraw(60);
    	 /** Show details of stuff in first index*/
    	 System.out.println(stuff[0].toString());
    	 /** Show details of stuff in second index*/
    	 System.out.println(stuff[1].toString());
    	 /** Show balance of the purse*/
    	 System.out.println(purse.getBalance());


    }
}