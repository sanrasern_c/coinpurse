package coinpurse;
/**
 * TO run the program.
 * @author Sanrasern
 *
 */
public class Ptest {
	public static void main(String[] arg){
		Purse p = new Purse(6);
		p.insert(new Coin(5 , "Baht"));
		p.insert(new Coin(2, "Baht"));
		p.insert(new Coin(3, "Baht"));
		
		System.out.println(p.getBalance());
		p.withdraw(5);
		System.out.println(p.getBalance());
	}
}
