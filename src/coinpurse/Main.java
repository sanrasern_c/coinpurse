package coinpurse;  

import java.util.ArrayList;

import coinpurse.strategy.GreedyWithdraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Sanrasern Chaihetphon 5710547247
 */
public class Main {

    /**
     * To run the program.
     * @param args not used
     * @throws ClassNotFoundException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    public static void main( String[] args ) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    	MoneyFactory moneyFactory = MoneyFactory.getInstance();
    	Valuable a = moneyFactory.createMoney(100);
    	System.out.print(a);
//    	GUIBalance guibalance = new GUIBalance() ;
//    	GUIStatus guistatus = new GUIStatus();
//    	
//    	
//      
//    	Purse purse = new Purse(20);
//    	purse.addObserver(guistatus);
//    	purse.addObserver(guibalance);
//    	purse.setWithdrawStrategy(new GreedyWithdraw());
//    	ConsoleDialog temp = new ConsoleDialog(purse);
//
//    	temp.run();
    }
}
