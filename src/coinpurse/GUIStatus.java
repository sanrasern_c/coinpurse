package coinpurse;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class GUIStatus extends JFrame implements Observer{
	
	/**
	 * showStatus is word to show that purse is full or not.
	 * proStatus is rate that show the used element in use.
	 */
	private JLabel showStatus;
	private JProgressBar proStatus;
	
	/**
	 * Contractor for this class. 
	 */
	public GUIStatus(){
		initcomponent();
		setVisible(true);
	}
	
	/**
	 * Method to show the showStatus and proStatus in GUI.
	 */
	private void initcomponent(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 100, 300, 150);
		
		JPanel pane = new JPanel();
		showStatus = new JLabel("EMPTY");
		
		setTitle("Purse Status Observer");
		proStatus = new JProgressBar();
		proStatus.setValue(0);
		
		pane.setLayout(new FlowLayout());
		pane.add(showStatus);
		pane.add(proStatus);
		
		proStatus.setStringPainted(true);

		add(pane);
		
	}

	/**
	 * Method to update the showStatus and proStatus.
	 */
	@Override
	public void update(Observable observe, Object info) {
		if(observe instanceof Purse){
			Purse purse = (Purse) observe;
			proStatus.setMaximum(purse.getCapacity());
			proStatus.setValue(purse.count());
			if(purse.count() == 0){
				showStatus.setText("EMPTY");
			}
			else if(purse.isFull()){
				showStatus.setText("FULL");
			}
			else{
				showStatus.setText(purse.count()+"");
			}
		}
		
	}

}
