package coinpurse;
/**
 * An abstract valuable with value.
 * @author Sanrasern Chaihetphon
 *
 */
public abstract class AbstractValuable implements Valuable {
	private String currency;
	public AbstractValuable(String currency) {
		super();
		this.currency = currency;
	}

	/**
	 * To check that this coin has same value to coin that want to check.
	 * @param arg is that coin want to check.
	 * @return True if 2 coins that check is a coin and have same value , if isn't return false.
	 */
	public boolean equals(Object arg) {
		if(arg == null)
			return false;
		if(arg.getClass() != this.getClass())
			return false;
		Valuable other = (Valuable) arg;
		if(this.getValue() == (other.getValue()))
			return true;
		return false;	
	}
	
	/**
     * Compare value of coin and return integer.
     * @param  coin2 is coin that we want to test.
     * @return integer more than 0 when value coin2 less than value of coin.
     * 		   integer less than 0 when value coin2 more than value of coin.
     *         integer  0 when value coin2 equals to value of coin.
     */
    public int compareTo(Valuable valuable){
    	if(this.getValue() > valuable.getValue())
    		return 1;
    	else if(this.getValue() == valuable.getValue())
    		return 0;
    	else 
    		return -1;
    }
    
    /**
     * Get the type of the money.
     * @return currency
     */
    public String getCurrency() {
    	return this.currency;
    }
}