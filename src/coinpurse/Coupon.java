package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * Coupon with a monetary value.
 * You can't change the value of a coupon.
 * @author Sanrasern5710547247
 *
 */
public class Coupon extends AbstractValuable{
	/**Color of the coupon*/
	private String color;
	
	/**
	 * Declare a map.
	 */
	Map<String,Double> map = new HashMap<String,Double>();
	
	 /** 
     * Constructor for a new coupon. 
     * @param color is the color for the coupon.
     */
	public Coupon(String color , String currency){
		super(currency);
		this.color = color.toLowerCase();
		map.put("red",100.0);
		map.put("blue",50.0);
		map.put("green",20.0);
	}
	/**
	 *Get the value of the coupon.
	 *@return value of coupon.
	 */
	public double getValue(){
		return map.get(this.color);
	}
	/**
	 * To return details of coupon. 
	 * @return String describing  the coupon.
	 */
	public String toString(){
		return this.color +  " coupon";
	}


}
