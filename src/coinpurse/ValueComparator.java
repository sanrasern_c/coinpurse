package coinpurse;
/**
 * To compare 2 objects of any Valuable
 * @author Sanrasern
 *
 */
import java.util.Comparator;
public class ValueComparator implements Comparator<Valuable> {
	public int compare(Valuable a, Valuable b) {
		if(a.getValue()<b.getValue())
			return -1;
		else if(a.getValue()==b.getValue())
			return 0;
		else 
			return 1;
		
	}
}
