package coinpurse;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * To make GUI to show balance .
 * @author Sanrasern Chaihetphon 5710547247
 */
public class GUIBalance extends JFrame implements Observer{
	/*
	 * showBalance is balance of purse.
	 */
	private JLabel showBalance;
	
	/**
	 * Contractor for this class.
	 */
	public GUIBalance(){
		initcomponent();
		setVisible(true);
	}
	
	/**
	 * Method to show the balance value in GUI.
	 */
	private void initcomponent(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0 , 400, 300, 150);
		JPanel pane = new JPanel();
		setTitle("Purse Balance");
		showBalance = new JLabel("0 Baht");
		pane.setLayout(new FlowLayout());
		pane.add(showBalance);
		add(pane);
	}
	
	/**
	 * Method to update the value of balance in purse.
	 */
	@Override
	public void update(Observable observe, Object info) {
		if(observe instanceof Purse){
			Purse purse = (Purse) observe;
			int balance = purse.getBalance();
			showBalance.setText(balance + " Baht");
		}
		
	}

}
