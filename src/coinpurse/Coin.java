package coinpurse ; 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author sanrasern5710547247
 */
public class Coin extends AbstractValuable {

    /** Value of the coin */
    double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin.
     */
    public Coin( double value, String currency ) {
    	super(currency);
        this.value = value;
    }

    /**
     * To check value of the coin.
     * @return double of value of the coin. 
     */
    public double getValue(){
    	return this.value;
    }

    
    /**
     * Display a description of what it withdraws.
     * @return  String description of Coin. 
     */
    public String toString(){
    	return (int)this.value  + " "+ this.getCurrency() +" coin";
    }

    
}

